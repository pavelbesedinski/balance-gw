FROM aleksandrmochalov/openjdk-11-jdk-slim-matlab-mcr-r2017a
COPY build/libs/balance-0.0.1-SNAPSHOT.jar .
EXPOSE 8080
ENV SPRING_PROFILES_ACTIVE=prod
ENTRYPOINT ["java","-jar","balance-0.0.1-SNAPSHOT.jar"]