package ru.gradwork.balance.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.gradwork.balance.model.BalanceInput;
import ru.gradwork.balance.model.BalanceOutput;
import ru.gradwork.balance.solver.SolverMode;
import ru.gradwork.balance.solver.SolverQP;

import javax.validation.Valid;

@Controller
@RequestMapping("balance")
public class Balance {
    public static final String SOLVER_MODE = "Solver";

    private final ObjectMapper objectMapper = new ObjectMapper();

    @RequestMapping(value = "post", method = RequestMethod.POST)
    public ResponseEntity<BalanceOutput> getBalance(@RequestBody @Valid BalanceInput balanceInput, @RequestHeader(SOLVER_MODE) SolverMode solverMode)  {
        try {
            SolverQP solver = new SolverQP(balanceInput);
            BalanceOutput balanceOutput = solver.solve(balanceInput.getBalanceSettings(), solverMode);
            return ResponseEntity.ok(balanceOutput);
        } catch (Exception e) {
            e.printStackTrace();
            return (ResponseEntity<BalanceOutput>) ResponseEntity.status(HttpStatus.BAD_REQUEST);
        }
    }
}
