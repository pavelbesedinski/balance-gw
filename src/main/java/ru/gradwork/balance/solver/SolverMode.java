package ru.gradwork.balance.solver;

public enum SolverMode {
    JOptimizer,
    OjAlgo,
    MATLAB,
    Gurobi
}
