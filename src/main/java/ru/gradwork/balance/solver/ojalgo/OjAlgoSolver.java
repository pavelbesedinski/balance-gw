package ru.gradwork.balance.solver.ojalgo;

import org.ojalgo.matrix.store.MatrixStore;
import org.ojalgo.matrix.store.Primitive32Store;
import org.ojalgo.optimisation.Optimisation;
import org.ojalgo.optimisation.convex.ConvexSolver;
import ru.gradwork.balance.solver.ISolver;

/*
    f(x) = 0.5 * [x^T][H][x] + [f^T][x] -> min
    WHERE f = [-H][x0]
    WHEN [A][x] = [b]
    добавить
 */
public class OjAlgoSolver implements ISolver {
        private void example() {
        try {
            MatrixStore<Double> Q = Primitive32Store.FACTORY.rows(new double[][]{{1, 0.4}, {0.4, 1}});
            MatrixStore<Double> C = Primitive32Store.FACTORY.column(new double[]{0, 0});

            MatrixStore<Double> Aeq = Primitive32Store.FACTORY.rows(new double[][]{{1, 1}});
            MatrixStore<Double> Beq = Primitive32Store.FACTORY.column(new double[]{1});


            MatrixStore<Double> Aineq = Primitive32Store.FACTORY.rows(new double[][]{{-1, 0}, {0, -1}});
            MatrixStore<Double> Bineq = Primitive32Store.FACTORY.column(new double[]{0, 0});


            ConvexSolver.Builder builder = new ConvexSolver.Builder(Q, C);
            builder.equalities(Aeq, Beq);
            builder.inequalities(Aineq, Bineq);
            ConvexSolver solver = builder.build();
            Optimisation.Result result = solver.solve();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public double[] solve(double[][] H, double[] f, double[][] Aeq, double[] beq, double[] lb, double[] ub, double[] x) {

        MatrixStore<Double> _H = Primitive32Store.FACTORY.rows(H);
        MatrixStore<Double> _d = Primitive32Store.FACTORY.column(f);

        MatrixStore<Double> _Aeq = Primitive32Store.FACTORY.rows(Aeq);
        MatrixStore<Double> _Beq = Primitive32Store.FACTORY.column(beq);

        double[][] Ain = new double[lb.length + ub.length][x.length];
        double[] Bin = new double[lb.length + ub.length];

        for (int i = 0; i < lb.length; i++) {
            for (int j = 0; j < x.length; j++) {
                if (i == j) Ain[i][j] = -1.0;
            }
            Bin[i] = lb[i];
        }

        for (int i = 0; i < ub.length; i++) {
            for (int j = 0; j < x.length; j++) {
                if (lb.length + i == j + lb.length) Ain[lb.length + i][j] = 1.0;
            }
            Bin[lb.length + i] = ub[i];
        }

        MatrixStore<Double> Aineq = Primitive32Store.FACTORY.rows(Ain);
        MatrixStore<Double> Bineq = Primitive32Store.FACTORY.columns(Bin);
        ConvexSolver.Builder builder = new ConvexSolver.Builder(_H, _d);
        builder.equalities(_Aeq, _Beq);
        builder.inequalities(Aineq, Bineq);
        ConvexSolver solver = builder.build();
        try {
            Optimisation.Result result = solver.solve();
            double[] solution = new double[x.length];
            for (int i = 0; i < solution.length; i++) {
                solution[i] = result.get(i).doubleValue();
            }
            return solution;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
