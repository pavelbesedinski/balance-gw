package ru.gradwork.balance.solver.joptimizer;

import com.joptimizer.exception.JOptimizerException;
import com.joptimizer.functions.ConvexMultivariateRealFunction;
import com.joptimizer.functions.LinearMultivariateRealFunction;
import com.joptimizer.functions.PDQuadraticMultivariateRealFunction;
import com.joptimizer.optimizers.JOptimizer;
import com.joptimizer.optimizers.OptimizationRequest;
import ru.gradwork.balance.solver.ISolver;

import java.util.ArrayList;

/*
    f(x) = 0.5 * [x^T][H][x] + [f^T][x] -> min
    WHERE f = [-H][x0]
    WHEN [A][x] = [b]
    добавить
 */
public class JOptimizerSolver implements ISolver {

    private void example() {

        // Objective function
        double[][] P = new double[][] {{ 1., 0.4 }, { 0.4, 1. }};
        PDQuadraticMultivariateRealFunction objectiveFunction = new PDQuadraticMultivariateRealFunction(P, null, 0);

        //equalities
        double[][] A = new double[][]{{1,1}};
        double[] b = new double[]{1};

        //inequalities
        ConvexMultivariateRealFunction[] inequalities = new ConvexMultivariateRealFunction[2];
        inequalities[0] = new LinearMultivariateRealFunction(new double[]{-1, 0}, 0);
        inequalities[1] = new LinearMultivariateRealFunction(new double[]{0, -1}, 0);

        //optimization problem
        OptimizationRequest or = new OptimizationRequest();
        or.setF0(objectiveFunction);
        or.setInitialPoint(new double[] { 0.1, 0.9});
        or.setFi(inequalities); //if you want x>0 and y>0
        or.setA(A);
        or.setB(b);
        or.setToleranceFeas(1.E-12);
        or.setTolerance(1.E-12);

        try {
            //optimization
            JOptimizer opt = new JOptimizer();
            opt.setOptimizationRequest(or);
            opt.optimize();
        } catch (JOptimizerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public double[] solve(double[][] H, double[] f, double[][] Aeq, double[] beq, double[] lb, double[] ub, double[] x) {
        for (int i = 0; i < f.length; i++) {
            f[i] *= -1;
        }

        PDQuadraticMultivariateRealFunction objectiveFunction = new PDQuadraticMultivariateRealFunction(H, f, 0);

        OptimizationRequest or = new OptimizationRequest();
        or.setF0(objectiveFunction);

        ArrayList<ConvexMultivariateRealFunction> inequalities = new ArrayList<ConvexMultivariateRealFunction>();

        for (int i = 0; i < x.length; i++) {
            double[] values = new double[x.length];
            values[i] = -1;
            inequalities.add(new LinearMultivariateRealFunction(values, lb[i]));
        }

//        for (int i = 0; i < x.length; i++) {
//            double[] values = new double[x.length];
//            values[i] = 1;
//            inequalities.add(new LinearMultivariateRealFunction(values, ub[i]));
//        }

        or.setFi(inequalities.toArray(new ConvexMultivariateRealFunction[0]));
        or.setA(Aeq);
        or.setB(beq);
        or.setToleranceFeas(1.E-12);
        or.setToleranceKKT(1.E-12);
        or.setTolerance(1.E-12);
        or.setToleranceInnerStep(1.E-12);
        or.setCheckKKTSolutionAccuracy(false);

        JOptimizer opt = new JOptimizer();
        opt.setOptimizationRequest(or);

        try {
            opt.optimize();
            return opt.getOptimizationResponse().getSolution();
        } catch (JOptimizerException e) {
            e.printStackTrace();
        }
        return null;
    }
}
