package ru.gradwork.balance.solver.matrix;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Matrix {
    public enum ParallelType {Stream, Thread, Standard};

    private double[][] matrix;

    public Matrix(int rows, int columns) {
        setSize(rows, columns);
    }

    public Matrix(int rows) {
        int columns = 1;
        setSize(rows, columns);
    }

    public Matrix(double[][] matrix) {
        this.matrix = new double[matrix.length][];
        for (int i = 0; i < matrix.length; i++) {
            this.matrix[i] = new double[matrix[i].length];
            this.matrix[i] = Arrays.copyOfRange(matrix[i], 0, matrix[i].length);
        }
    }

    public MatrixSize getSize() {
        return new MatrixSize(matrix.length, matrix[0].length);
    }

    void setSize(int rows, int columns) {
        matrix = new double[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                matrix[i][j] = 0.0;
            }
        }
    }

    public void setValue(int row, int column, double value) {
        matrix[row][column] = value;
    }

    public void setValue(double[][] array) {
        if (array == null) throw new NullPointerException("Array can't be null");
        try {
            if (matrix.length != array.length || matrix[0].length != array[0].length) {
                String exceptionString = "Size of array isn't correct. ";
                if (matrix.length != array.length) {
                    exceptionString += "Expected " + matrix.length + " but found " + array.length;
                } else if (matrix[0].length != array[0].length) {
                    exceptionString += "Expected " + matrix[0].length + " but found " + array[0].length;
                }
                throw new Exception(exceptionString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                this.matrix[i][j] = array[i][j];
            }
        }
    }

    public double getValue(int row, int column) {
        return matrix[row][column];
    }

    public Matrix multiplication(Matrix matrix, ParallelType parallelType) throws Exception {
        try {
            switch (parallelType) {
                case Stream: {
                    double[][] incomeMatrix = matrix.getMatrixToArray();
                    double[][] results = Arrays.stream(this.matrix).map(r ->
                            IntStream.range(0, incomeMatrix[0].length).mapToDouble(i ->
                                    IntStream.range(0, incomeMatrix.length).mapToDouble(j -> r[j] * incomeMatrix[j][i]).sum()
                            ).toArray()).toArray(double[][]::new);
                    return new Matrix(results);
                }
                case Thread: {
                    double[][] incomeMatrix = matrix.getMatrixToArray();
                    ParallelMatrix parallelMatrix = new ParallelMatrix(this.matrix, incomeMatrix, Runtime.getRuntime().availableProcessors());
                    double[][] results = parallelMatrix.parallelProduct();
                    return new Matrix(results);
                }
                case Standard: {
                    return multiplication(matrix);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new Exception("Exception: Failed to multiplicate!");
        }
        return null;
    }

    public Matrix multiplication(Matrix matrix) {
        if (matrix == null) throw new NullPointerException("Matrix can't be null");
        double[][] incomeMatrix = matrix.getMatrixToArray();

        int rows = getSize().getRows();
        int cols = getSize().getColumns();

        int rowsIn = matrix.getSize().getRows();
        int colsIn = matrix.getSize().getColumns();
        double tmp;
        try {
            if (cols == rowsIn) {
                double[][] resultMatrix = new double[rows][colsIn];
                for (int i = 0; i < rows; i++) {
                    for (int j = 0; j < colsIn; j++) {
                        resultMatrix[i][j] = 0.0;
                        tmp = 0.0;
                        for (int k = 0; k < cols; k++) {
                            if (Double.isNaN(this.matrix[i][k]) || Double.isNaN(incomeMatrix[k][j])) {
                                throw new Exception("Matrix Exception: NaN Value !");
                            }
                            if (Double.isInfinite(this.matrix[i][k]) || Double.isInfinite(incomeMatrix[k][j])) {
                                throw new Exception("Matrix Exception: Infinity Value !");
                            }
                            tmp += this.matrix[i][k] * incomeMatrix[k][j];
                            if (Double.isNaN(tmp)) {
                                throw new Exception("Matrix NaN value !");
                            }
                        }

                        resultMatrix[i][j] = tmp;
                    }
                }
                return new Matrix(resultMatrix);
            } else {
                throw new Exception("Error: The count of columns in the first matrix should be equal to the count of rows "
                        + "in the second matrix");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

//    public Matrix multiplication(Matrix matrix) {
//        if (matrix == null) throw new NullPointerException("Matrix can't be null");
//        int rows = getSize().getRows();
//        int cols = getSize().getColumns();
//
//        int rowsIn = matrix.getSize().getRows();
//        int colsIn = matrix.getSize().getColumns();
//
//        double tmp;
//        try {
//            if (cols == rowsIn) {
//                Matrix resMatrix = new Matrix(rows, colsIn);
//                for (int i = 0; i < rows; i++) {
//                    for (int j = 0; j < colsIn; j++) {
//                        resMatrix.setValue(i, j, 0.0);
//                        tmp = 0.0;
//                        for (int k = 0; k < cols; k++) {
//                            tmp += getValue(i, k) * matrix.getValue(k, j);
//                        }
//                        resMatrix.setValue(i, j, tmp);
//                    }
//                }
//                return resMatrix;
//            } else {
//                throw new Exception("Error: The count of columns in the first matrix should be equal to the count of rows "
//                        + "in the second matrix");
//            }
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//            return null;
//        }
//    }

    public Matrix transpose() {
        Matrix transposedMatrix = new Matrix(matrix[0].length, matrix.length);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                transposedMatrix.setValue(j, i, matrix[i][j]);
            }
        }
        return transposedMatrix;
    }

    public double[][] getMatrixToArray() {
        return matrix;
    }

    public double[] getVectorToArray() {
        double[] vector = new double[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            vector[i] = matrix[i][0];
        }
        return vector;
    }
}
