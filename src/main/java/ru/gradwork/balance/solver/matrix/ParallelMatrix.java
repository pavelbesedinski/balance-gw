package ru.gradwork.balance.solver.matrix;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ParallelMatrix {
    private int threadsNumber;
    private double[][] a;
    private double[][] b;
    private double[][] rez;

    private class myThread implements Runnable{
        private int index;

        public myThread(int index) {
            this.index = index;
        }
        @Override
        public void run(){
            for (int j = 0; j < b[0].length; j++) {
                for (int k = 0; k < a[0].length; k++) {
                    rez[this.index][j] += a[this.index][k] * b[k][j];
                }
            }
        }
    }

    ParallelMatrix(double[][] a, double[][] b, int threadsNumber){
        this.a = a;
        this.b = b;
        this.threadsNumber = threadsNumber;
        this.rez = new double[a.length][b[0].length];
    }

    public double[][] parallelProduct() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(this.threadsNumber);
        for(int i = 0; i < a.length; i++)
            executorService.execute(new myThread(i));
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
        return this.rez;
    }
}
