package ru.gradwork.balance.solver;

import ru.gradwork.balance.model.*;
import ru.gradwork.balance.solver.gurobi.GurobiSolver;
import ru.gradwork.balance.solver.joptimizer.JOptimizerSolver;
import ru.gradwork.balance.solver.matlab.MatLabSolver;
import ru.gradwork.balance.solver.matrix.Matrix;
import ru.gradwork.balance.solver.ojalgo.OjAlgoSolver;
import ru.gradwork.balance.utils.solver.SolverUtils;
import ru.gradwork.balance.utils.solver.SolverUtilsException;

import java.util.ArrayList;

public class SolverQP {
    private int countOfThreads; // Количество потоков

    private Matrix measuredValues;              // Вектор измеренных значений (x0)
    private Matrix measureIndicator;            // Матрица измеряемости (I)
    private Matrix standardDeviation;           // Матрица метрологической погрешности (W)
    private Matrix incidenceMatrix;             // Матрица инцидентности / связей
    private Matrix reconciledValues;            // Вектор сбалансированных значений (x)
    private Matrix metrologicRangeUpperBound;   // Вектор верхних ограничений вектора x
    private Matrix metrologicRangeLowerBound;   // Вектор нижних ограничений вектора x
    private Matrix technologicRangeUpperBound;  // Вектор верхних ограничений вектора x
    private Matrix technologicRangeLowerBound;  // Вектор нижних ограничений вектора x
    private Matrix H;                           // H = I * W
    private Matrix dVector;                     // d = H * x0

    private BalanceInput inputData;
    public SolverQP(BalanceInput balanceInput) {
        try {


            this.inputData = balanceInput;
            // Инициализация количества потоков
            countOfThreads = balanceInput.getBalanceInputVariables().size();
            // Инициализация матрицы инцидентности ( A )
            SolverUtils solverUtils = new SolverUtils();
            incidenceMatrix = new Matrix(solverUtils.getIncidenceMatrix(balanceInput).length, solverUtils.getIncidenceMatrix(balanceInput)[0].length);
            incidenceMatrix.setValue(solverUtils.getIncidenceMatrix(balanceInput));
            // Инициализация вектора измеренных значений ( x0 )
            measuredValues = new Matrix(countOfThreads);
            // Инициализация матрицы измеряемости ( I )
            measureIndicator = new Matrix(countOfThreads, countOfThreads);
            // Инициализация матрицы метрологической погрешности ( 1 / t * t )
            standardDeviation = new Matrix(countOfThreads, countOfThreads);
            // Инициализация вектора верхних ограничений вектора x
            metrologicRangeUpperBound = new Matrix(countOfThreads);
            technologicRangeUpperBound = new Matrix(countOfThreads);
            // Инициализация вектора нижних ограничений вектора x
            metrologicRangeLowerBound = new Matrix(countOfThreads);
            technologicRangeLowerBound = new Matrix(countOfThreads);

            for (int i = 0; i < countOfThreads; i++) {
                BalanceInputVariable variables = balanceInput.getBalanceInputVariables().get(i);
                // Определение вектора измеренных значений
                measuredValues.setValue(i, 0, variables.getMeasured());
                // Определение матрицы измеряемости
                if (variables.getIsMeasured()) measureIndicator.setValue(i, i, 1.0);
                else measureIndicator.setValue(i, i, 0.0);
                // Определение матрицы метрологической погрешности
                if (!variables.getIsMeasured()) standardDeviation.setValue(i, i, 1.0);
                else {
                    double tolerance = 1.0 / Math.pow(variables.getTolerance(), 2);
                    if (Double.isInfinite(tolerance)) {
                        tolerance = 1.0;
//                        throw new Exception("Exception: Infinite Value");
                    }
                    if (Double.isNaN(tolerance)) {
                        throw new Exception("Exception: NaN Value");
                    }
                    standardDeviation.setValue(i, i, tolerance);
                }
                // Определение вектора верхних ограничений вектора x
                metrologicRangeUpperBound.setValue(i, 0, variables.getMetrologicRange().getUpperBound());
                technologicRangeUpperBound.setValue(i, 0, variables.getTechnologicRange().getUpperBound());
                // Определение вектора нижних ограничений вектора x
                metrologicRangeLowerBound.setValue(i, 0, variables.getMetrologicRange().getLowerBound());
                technologicRangeLowerBound.setValue(i, 0, variables.getTechnologicRange().getLowerBound());
            }

            H = measureIndicator.multiplication(this.standardDeviation, Matrix.ParallelType.Thread);
            dVector = H.multiplication(measuredValues, Matrix.ParallelType.Thread);
            // Инициализация вектора сбалансированных значений
            reconciledValues = new Matrix(incidenceMatrix.getSize().getRows());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public BalanceOutput solve(BalanceSettings balanceSettings, SolverMode solverMode) throws Exception {
        ISolver solver = null;
        switch (solverMode) {
            case JOptimizer: {
                solver = new JOptimizerSolver();
            } break;
            case OjAlgo: {
                solver = new OjAlgoSolver();
            } break;
            case MATLAB: {
                solver = new MatLabSolver();
            } break;
            case Gurobi: {
                solver = new GurobiSolver();
            }
        }
        double[] solution = null;
        switch (balanceSettings.getBalanceSettingsConstraints()) {
            case METROLOGIC: {
                solution = solver.solve(H.getMatrixToArray(), dVector.getVectorToArray(), incidenceMatrix.getMatrixToArray(), reconciledValues.getVectorToArray(), metrologicRangeLowerBound.getVectorToArray(), metrologicRangeUpperBound.getVectorToArray(), measuredValues.getVectorToArray());
            } break;
            case TECHNOLOGIC: {
                solution = solver.solve(H.getMatrixToArray(), dVector.getVectorToArray(), incidenceMatrix.getMatrixToArray(), reconciledValues.getVectorToArray(), technologicRangeLowerBound.getVectorToArray(), technologicRangeUpperBound.getVectorToArray(), measuredValues.getVectorToArray());
            } break;
        }

        BalanceOutput balanceOutput = new BalanceOutput();
        try {
            double delta = 1.0 / 1000.0;
            double imbalance = new SolverUtils().getImbalance(this.incidenceMatrix.getMatrixToArray(), solution, delta);

            ArrayList<BalanceOutputVariable> balanceOutputVariables = new ArrayList<>();
            for (int i = 0; i < solution.length; i++) {
                BalanceInputVariable outputVariable = inputData.getBalanceInputVariables().get(i);
                balanceOutputVariables.add(new BalanceOutputVariable(outputVariable.getId(), outputVariable.getName(), solution[i], outputVariable.getSourceId(), outputVariable.getDestinationId()));
            }

            balanceOutput.setBalanceOutputVariables(balanceOutputVariables);
            balanceOutput.setSolverMode(solverMode);
            balanceOutput.setImbalance(imbalance);

        } catch (SolverUtilsException e) {
            e.printStackTrace();
            throw new Exception("Imbalance is " + e.getImbalance());
        }
        return balanceOutput;
    }
}
