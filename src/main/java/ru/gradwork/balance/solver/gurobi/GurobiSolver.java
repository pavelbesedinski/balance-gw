package ru.gradwork.balance.solver.gurobi;

import gurobi.*;
import ru.gradwork.balance.solver.ISolver;

public class GurobiSolver implements ISolver {
    @Override
    public double[] solve(double[][] H, double[] f, double[][] Aeq, double[] beq, double[] lb, double[] ub, double[] x) {
        double[][] _H = new double[H.length][H[0].length];
        for (int i = 0; i < _H.length; i++) {
            for (int j = 0; j < _H[i].length; j++) {
                _H[i][j] = H[i][j] / 2.0;
            }
        }
        for (int i = 0; i < f.length; i++) {
            f[i] *= -1.0;
        }
        try {
            GRBEnv env   = new GRBEnv();
            GRBModel model = new GRBModel(env);

            // Create variables
            GRBVar[] vars = new GRBVar[x.length];
            for (int i = 0; i < vars.length; i++) {
                vars[i] = model.addVar(lb[i], ub[i], 0.0, GRB.CONTINUOUS, "x" + i);
            }

            // Set objective
            GRBQuadExpr obj = new GRBQuadExpr();
            for (int i = 0; i < vars.length; i++) {
                obj.addTerm(_H[i][i], vars[i], vars[i]);
            }
            for (int i = 0; i < vars.length; i++) {
                obj.addTerm(f[i], vars[i]);
            }
            model.setObjective(obj);

            // Add constraints
            GRBLinExpr expr;
            for (int i = 0; i < Aeq.length; i++) {
                expr = new GRBLinExpr();
                for (int j = 0; j < Aeq[i].length; j++) {
                    expr.addTerm(Aeq[i][j], vars[j]);
                }
                model.addConstr(expr, GRB.EQUAL, 0.0, "c" + i);
            }

            // Optimize model
            model.optimize();

            for (int i = 0; i < vars.length; i++) {
                System.out.println(vars[i].get(GRB.StringAttr.VarName)
                    + " " +vars[i].get(GRB.DoubleAttr.X));
            }
            System.out.println("Obj: " + model.get(GRB.DoubleAttr.ObjVal) + " " +
                    obj.getValue());
            System.out.println();

            double[] results = new double[vars.length];
            for (int i = 0; i < results.length; i++) {
                results[i] = vars[i].get(GRB.DoubleAttr.X);
            }
            model.dispose();
            env.dispose();

            return results;
        } catch (GRBException e) {
            e.printStackTrace();
        }
        return null;
    }
}
