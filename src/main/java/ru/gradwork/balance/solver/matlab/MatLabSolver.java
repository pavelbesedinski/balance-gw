package ru.gradwork.balance.solver.matlab;

import MATLABSolver.MATLABQPSolver;
import com.mathworks.toolbox.javabuilder.MWNumericArray;
import ru.gradwork.balance.solver.ISolver;

public class MatLabSolver implements ISolver {

    private MWNumericArray createArray(double[] arr) {
        double[][] res = new double[arr.length][];
        for (int i = 0; i < arr.length; i++) {
            res[i] = new double[1];
            res[i][0] = arr[i];
        }
        return new MWNumericArray(res);
    }

    @Override
    public double[] solve(double[][] H, double[] f, double[][] Aeq, double[] beq, double[] lb, double[] ub, double[] x) {
        for (int i = 0; i < f.length; i++) {
            f[i] *= -1;
        }

        try {
            MWNumericArray _H = new MWNumericArray(H);
            MWNumericArray h = createArray(f);

            MWNumericArray Aequal = new MWNumericArray(Aeq);
            MWNumericArray Bequal = createArray(beq);

            MWNumericArray LowerBounds = createArray(lb);
            MWNumericArray UpperBounds = createArray(ub);

            MWNumericArray X0 = createArray(x);

            MWNumericArray MaxIter = new MWNumericArray(20000);
            MWNumericArray DrTol = new MWNumericArray(200);

            MATLABQPSolver QPSolver = new MATLABQPSolver();
            Object[] result = QPSolver.QPSolver(3, _H, h, Aequal, Bequal, LowerBounds, UpperBounds, X0, MaxIter);

            double[][] res = ((double[][])((MWNumericArray) (result[0])).toDoubleArray() );
            double[] outPut = new double[res.length];
            for (int i = 0; i < outPut.length; i++) {
                outPut[i] = res[i][0];
            }
            return outPut;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new double[0];
    }
}
