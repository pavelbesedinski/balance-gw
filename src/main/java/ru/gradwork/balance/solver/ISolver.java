package ru.gradwork.balance.solver;

import ru.gradwork.balance.solver.matrix.Matrix;

public interface ISolver {
    double[] solve(double[][] H, double[] f, double[][] Aeq, double[] beq, double[] lb, double[] ub, double[] x);
}
