package ru.gradwork.balance.model;

public enum BalanceSettingsConstraints {
    METROLOGIC,
    TECHNOLOGIC,
    METROLOGIC_AND_TECHNOLOGIC
}
