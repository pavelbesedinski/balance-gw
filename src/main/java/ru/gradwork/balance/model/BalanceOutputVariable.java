package ru.gradwork.balance.model;

public class BalanceOutputVariable {
    private String id;
    private double value;
    private String name;
    private String source;
    private String target;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public BalanceOutputVariable (String id, String name, double value, String source, String target) {
        setId(id);
        setValue(value);
        setName(name);
        setSource(source);
        setTarget(target);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
