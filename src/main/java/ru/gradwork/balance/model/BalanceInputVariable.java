package ru.gradwork.balance.model;

public class BalanceInputVariable {
    private String id;
    private String sourceId;
    private String destinationId;
    private String name;

    private double measured;
    private BalanceConstraints metrologicRange;
    private BalanceConstraints technologicRange;
    private double tolerance;

    private boolean isMeasured;
    private boolean isExcluded;

    public void setTestValues(String id, String sourceId, String destinationId, double measured, BalanceConstraints metrologicRange, BalanceConstraints technologicRange, double tolerance, boolean isMeasured, boolean isExcluded) {
        setId(id);
        setSourceId(sourceId);
        setDestinationId(destinationId);
        setMeasured(measured);
        setMetrologicRange(new BalanceConstraints(metrologicRange.getLowerBound(), metrologicRange.getUpperBound()));
        setTechnologicRange(new BalanceConstraints(technologicRange.getLowerBound(), technologicRange.getUpperBound()));
        setTolerance(tolerance);
        setIsMeasured(isMeasured);
        setIsExcluded(isExcluded);
    }

    public double getMeasured() {
        return measured;
    }

    public void setMeasured(double measured) {
        this.measured = measured;
    }

    public double getTolerance() {
        return tolerance;
    }

    public void setTolerance(double tolerance) {
        this.tolerance = tolerance;
    }

    public boolean getIsExcluded() {
        return isExcluded;
    }

    public void setIsExcluded(boolean isExcluded) {
        this.isExcluded = isExcluded;
    }

    public boolean getIsMeasured() {
        return isMeasured;
    }

    public void setIsMeasured(boolean isMeasured) {
        this.isMeasured = isMeasured;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BalanceConstraints getMetrologicRange() {
        return metrologicRange;
    }

    public void setMetrologicRange(BalanceConstraints metrologicRange) {
        this.metrologicRange = metrologicRange;
    }

    public BalanceConstraints getTechnologicRange() {
        return technologicRange;
    }

    public void setTechnologicRange(BalanceConstraints technologicRange) {
        this.technologicRange = technologicRange;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }
}
