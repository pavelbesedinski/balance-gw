package ru.gradwork.balance.model;

import ru.gradwork.balance.solver.SolverMode;

import java.util.ArrayList;

public class BalanceOutput {
    private ArrayList<BalanceOutputVariable> balanceOutputVariables;
    private SolverMode solverMode;
    private double imbalance;

    public ArrayList<BalanceOutputVariable> getBalanceOutputVariables() {
        return balanceOutputVariables;
    }

    public void setBalanceOutputVariables(ArrayList<BalanceOutputVariable> balanceOutputVariables) {
        this.balanceOutputVariables = balanceOutputVariables;
    }

    public SolverMode getSolverMode() {
        return solverMode;
    }

    public void setSolverMode(SolverMode solverMode) {
        this.solverMode = solverMode;
    }

    public double getImbalance() {
        return imbalance;
    }

    public void setImbalance(double imbalance) {
        this.imbalance = imbalance;
    }
}
