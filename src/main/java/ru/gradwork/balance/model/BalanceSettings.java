package ru.gradwork.balance.model;

public class BalanceSettings {
    private double roundUnit;

    private BalanceSettingsConstraints balanceSettingsConstraints;

    public void setRoundUnit(double roundUnit) {
        this.roundUnit = roundUnit;
    }

    public double getRoundUnit() {
        return roundUnit;
    }

    public BalanceSettingsConstraints getBalanceSettingsConstraints() {
        return balanceSettingsConstraints;
    }

    public void setBalanceSettingsConstraints(BalanceSettingsConstraints balanceSettingsConstraints) {
        this.balanceSettingsConstraints = balanceSettingsConstraints;
    }
}
