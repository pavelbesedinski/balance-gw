package ru.gradwork.balance.model;

public class BalanceConstraints {
    private double upperBound;
    private double lowerBound;
    public BalanceConstraints () {
        setLowerBound(0.0);
        setUpperBound(0.0);
    }
    public BalanceConstraints (double lowerBound, double upperBound) {
        setLowerBound(lowerBound);
        setUpperBound(upperBound);
    }
    public double getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(double upperBound) {
        this.upperBound = upperBound;
    }

    public double getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(double lowerBound) {
        this.lowerBound = lowerBound;
    }
}
