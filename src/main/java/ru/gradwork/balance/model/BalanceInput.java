package ru.gradwork.balance.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.ArrayList;

public class BalanceInput {
    private ArrayList<BalanceInputVariable>  balanceInputVariables;
    private BalanceSettings balanceSettings;

    public BalanceInput() {
        this.balanceSettings = new BalanceSettings();
        this.balanceInputVariables = new ArrayList<>();
    }

    public ArrayList<BalanceInputVariable> getBalanceInputVariables() {
        ArrayList<BalanceInputVariable>  balanceInputVariables = new ArrayList<>();
        for (BalanceInputVariable balanceInputVariable: this.balanceInputVariables
             ) {
            if (!balanceInputVariable.getIsExcluded()) {
                balanceInputVariables.add(balanceInputVariable);
            }
        }
        return balanceInputVariables;
    }

    public void setBalanceInputVariables(ArrayList<BalanceInputVariable> balanceInputVariables) {
        this.balanceInputVariables = balanceInputVariables;
    }

    public String getJsonBalanceInputList() {
        String result = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            for (BalanceInputVariable balanceInputVariable : balanceInputVariables) {
                result += objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                        .writeValueAsString(balanceInputVariable);
            }
        } catch (JsonProcessingException e) {
            result = e.toString();
        }
        return result;
    }

    public BalanceSettings getBalanceSettings() {
        return balanceSettings;
    }

    public void setBalanceSettings(BalanceSettings balanceSettings) {
        this.balanceSettings = balanceSettings;
    }
}
