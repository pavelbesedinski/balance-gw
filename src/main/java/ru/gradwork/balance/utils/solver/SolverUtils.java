package ru.gradwork.balance.utils.solver;

import ru.gradwork.balance.model.BalanceInput;
import ru.gradwork.balance.utils.graph.Graph;

public class SolverUtils {

    public double[][] getIncidenceMatrix(BalanceInput balanceInput) {
        try{
            Graph graph = new Graph(balanceInput);
            return graph.getMatrix();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public double getImbalance(double[][] Arr, double[] value, double maximumDeviation) throws SolverUtilsException {
        if (value.length == 0) throw new SolverUtilsException("The length of the solution array is 0");
        double sum = 0.0;
        for (int i = 0; i < Arr[0].length; i++) {
            for (int j = 0; j < Arr.length; j++) {
                sum += Arr[j][i] * value[i];
            }
        }
        if (sum > maximumDeviation) throw new SolverUtilsException("Imbalance exceeds the limit", sum);
        return sum;
    }
}
