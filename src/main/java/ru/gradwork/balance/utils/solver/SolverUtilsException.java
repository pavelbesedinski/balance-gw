package ru.gradwork.balance.utils.solver;

public class SolverUtilsException extends Exception {
    private double imbalance;

    SolverUtilsException(String message) {
        super(message);
    }

    SolverUtilsException(String message, double value) {
        super(message);
        imbalance = value;
    }

    public double getImbalance() {
        return imbalance;
    }
}
