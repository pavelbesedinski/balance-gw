package ru.gradwork.balance.utils.graph;

public class Vertex {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Vertex(String id)
    {
        this.id = id;
    }
}
