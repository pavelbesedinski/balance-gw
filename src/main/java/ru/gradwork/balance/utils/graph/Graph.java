package ru.gradwork.balance.utils.graph;

import ru.gradwork.balance.model.BalanceInput;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Класс описание графа
 */
public class Graph {

    // Список вершин
    private ArrayList<Vertex> vertexList;
    // Матрица инцидентности
    private double[][] matrix;
    public Graph(BalanceInput balanceInput) throws Exception {
        if (balanceInput == null) throw new NullPointerException("balanceInput can't be null");

        vertexList = new ArrayList<>();

        // Определение количества потоков
        int countOfThreads = balanceInput.getBalanceInputVariables().size();

        // Создание вершин графа
        for (int i = 0; i < countOfThreads; i++)
        {
            String sourceId = balanceInput.getBalanceInputVariables().get(i).getSourceId();
            String destinationId = balanceInput.getBalanceInputVariables().get(i).getDestinationId();
            if (sourceId != null)
            {
                boolean isExisted = false;
                for (Vertex vertex : vertexList) {
                    if (sourceId.equals(vertex.getId())) {
                        isExisted = true;
                        break;
                    }
                }
                if (!isExisted)
                {
                    vertexList.add(new Vertex(sourceId));
                }
            }
            else if (destinationId != null)
            {
                boolean isExisted = false;
                for (Vertex vertex : vertexList) {
                    if (destinationId.equals(vertex.getId())) {
                        isExisted = true;
                        break;
                    }
                }
                if (!isExisted)
                {
                    vertexList.add(new Vertex(destinationId));
                }
            }
        }

        // Определение количества вершин
        int countOfVertexes = vertexList.size();

        // Инициализация матрицы инцидентности
        matrix = new double[countOfVertexes][countOfThreads];
        for (int thread = 0; thread < countOfThreads; thread ++) {
            String sourceId = balanceInput.getBalanceInputVariables().get(thread).getSourceId();
            String destinationId = balanceInput.getBalanceInputVariables().get(thread).getDestinationId();

            for (int vertex = 0; vertex < vertexList.size(); vertex++) {
                String vertexId = vertexList.get(vertex).getId();
                if (destinationId != null && destinationId.equals(vertexId)) {
                    matrix[vertex][thread] = 1;
                }
                if (sourceId != null && sourceId.equals(vertexId)) {
                    matrix[vertex][thread] = -1;
                }
            }
        }
    }
    public double[][] getMatrix() {
        return matrix;
    }
}
