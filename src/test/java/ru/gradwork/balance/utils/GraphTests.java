package ru.gradwork.balance.utils;

import org.junit.jupiter.api.Test;
import ru.gradwork.balance.model.BalanceConstraints;
import ru.gradwork.balance.model.BalanceInput;
import ru.gradwork.balance.model.BalanceInputVariable;
import ru.gradwork.balance.model.BalanceSettings;

import java.util.ArrayList;
import java.util.Arrays;

public class GraphTests {
    private BalanceSettings expectedBalanceSettings = null;
    private BalanceInput expectedBalanceInput = null;

    @Test
//    @Before
    public void testBalanceInputInit() {
        // Setting for expectedBalanceSetting
        this.expectedBalanceSettings = new BalanceSettings();
        expectedBalanceSettings.setRoundUnit(0.01);

        BalanceInputVariable[] expectedBalanceInputVariable = new BalanceInputVariable[7];
        for (int i = 0; i < 7; i++) {
            expectedBalanceInputVariable[i] = new BalanceInputVariable();
        }
        expectedBalanceInputVariable[0].setTestValues(String.valueOf(1), null, String.valueOf(1), 10.005,  new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.2, true, false);
        expectedBalanceInputVariable[1].setTestValues(String.valueOf(2), String.valueOf(1), null, 3.033, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.121, true, false);
        expectedBalanceInputVariable[2].setTestValues(String.valueOf(3), String.valueOf(1), String.valueOf(2), 6.831, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.683, true, false);
        expectedBalanceInputVariable[3].setTestValues(String.valueOf(4), String.valueOf(2), null, 1.985, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.04, true, false);
        expectedBalanceInputVariable[4].setTestValues(String.valueOf(5), String.valueOf(2), String.valueOf(3), 5.093, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.102, true, false);
        expectedBalanceInputVariable[5].setTestValues(String.valueOf(6), String.valueOf(3), null, 4.057, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.081, true, false);
        expectedBalanceInputVariable[6].setTestValues(String.valueOf(7), String.valueOf(3), null, 0.991, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.02, true, false);

        // Setting for expectedBalanceInputVariable
        ArrayList<BalanceInputVariable> expectedBalanceInputVariableList = new ArrayList<>(Arrays.asList(expectedBalanceInputVariable).subList(0, 7));

        // Setting for expectedBalanceInput
        this.expectedBalanceInput = new BalanceInput();
        expectedBalanceInput.setBalanceSettings(expectedBalanceSettings);
        expectedBalanceInput.setBalanceInputVariables(expectedBalanceInputVariableList);
    }

    @Test
    public void testGraphCreate() {
//        Graph graph = new Graph(expectedBalanceInput);
//        double[][] actual = graph.getMatrix();
//        Matrix expectedMatrix = new Matrix(3, 7);
//        expectedMatrix.setValue(new double[][]{
//                {1, -1, -1, 0, 0, 0, 0},
//                {0, 0, 1, -1, -1, 0, 0},
//                {0, 0, 0, 0, 1, -1, -1}
//        });
//        double[][] expected = expectedMatrix.getMatrixToArray();
//
//        for (int i = 0; i < expected.length; i++) {
//            for (int j = 0; j < expected[0].length; j++) {
//                assertEquals(expected[i][j],actual[i][j], 0);
//            }
//        }
    }
}
