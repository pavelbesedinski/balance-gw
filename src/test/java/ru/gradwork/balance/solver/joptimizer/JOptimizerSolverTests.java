package ru.gradwork.balance.solver.joptimizer;

//import org.junit.Before;

import org.junit.jupiter.api.Test;
import ru.gradwork.balance.solver.matrix.Matrix;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JOptimizerSolverTests {
    public static final double INACCURACY_LIMIT = 1.0E-3;
    double[][] Aeq;
    double[] beq;
    double[] lowerBounds;
    double[] upperBounds;
    double[][] measuredFlows;
    double[] tolerances;
    double[] x0;

//    @Before
    public void testJOptimizerInit() {
        x0 = new double[] {10.005, 3.033, 6.831, 1.985, 5.093, 4.057, 0.991};
        Aeq = new double[][]
                {
                        {1, -1, -1, 0, 0, 0, 0},
                        {0, 0, 1, -1, -1, 0, 0},
                        {0, 0, 0, 0, 1, -1, -1},
                };
        beq = new double[] {0, 0, 0};
        lowerBounds = new double[] {0, 0, 0, 0, 0, 0, 0};
        upperBounds = new double[] {10000, 10000, 10000, 10000, 10000, 10000, 10000};
        measuredFlows = new double[][] {
                {1, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 0, 1}
        };
        tolerances = new double[] {0.200, 0.121, 0.683, 0.040, 0.102, 0.081, 0.020};
    }

    @Test
    public void testJOptimizerSolver() {
        testJOptimizerInit();
        JOptimizerSolver jOptimizerSolver = new JOptimizerSolver();

        Matrix I = new Matrix(Aeq[0].length, Aeq[0].length);
        Matrix W = new Matrix(Aeq[0].length, Aeq[0].length);
        Matrix x0 = new Matrix(Aeq[0].length);
        I.setValue(measuredFlows);
        for (int i = 0; i < Aeq[0].length; i++) {
            W.setValue(i,i, 1 / Math.pow(tolerances[i], 2));
            x0.setValue(i, 0, this.x0[i]);
        }
        Matrix H = I.multiplication(W);
        Matrix f = H.multiplication(x0);
        double[] actual = jOptimizerSolver.solve(H.getMatrixToArray(), f.getVectorToArray(), Aeq, beq, lowerBounds, upperBounds, this.x0);
        double[] expected = new double[]{10.056, 3.014, 7.041, 1.982, 5.059, 4.067, 0.992};
        for (int i = 0; i < expected.length; i++) {
                assertEquals(expected[i],actual[i], INACCURACY_LIMIT);
        }
    }
}
