package ru.gradwork.balance.solver.matrix;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MatrixTests {
    private final double[][] matrix1 = new double[][] {{1.5, 2.5, 3.5}, {2.5, 3.5, 4.5}, {3.5, 4.5, 5.5}};
    private final double[][] matrix1AndMatrix1 = new double[][] {{20.75, 28.25, 35.75}, {28.25, 38.75, 49.25}, {35.75, 49.25, 62.75}};
    private final double[][] matrix2 = new double[][] {{1.6, 2.6}, {3.9, 4.9}, {5.1, 6.1}};
    private final double[][] matrix3 = new double[][] {{2.345, 3.567, 23.2}, {67.2, 34.56, 11.2}};
    private final double[][] matrix2AndMatrix3 = new double[][] {{178.472, 95.5632, 66.24}, {338.4255, 183.2553, 145.36}, {421.8795, 229.0077, 186.64}};

    private final double INACCURACY_VALUE = 1.0E-7;

    @Test
    public void testCreateMatrix() {
        Matrix matrixWithConstructorThree = new Matrix(matrix1);
        double[][] expected = new double[][] {{1.5, 2.5, 3.5}, {2.5, 3.5, 4.5}, {3.5, 4.5, 5.5}};
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[i].length; j++) {
                assertEquals(expected[i][j], matrixWithConstructorThree.getMatrixToArray()[i][j], INACCURACY_VALUE);
            }
        }
        assertEquals(expected.length, matrixWithConstructorThree.getSize().getRows(), INACCURACY_VALUE);
        assertEquals(expected[0].length, matrixWithConstructorThree.getSize().getColumns(), INACCURACY_VALUE);
    }

    @Test
    public void testMatrixMultiplication1() {
        Matrix matrixMul1 = new Matrix(matrix1);
        Matrix matrixMul2 = new Matrix(matrix1);
        Matrix matrixRes = new Matrix(matrix1AndMatrix1);

        double[][] actual = matrixMul1.multiplication(matrixMul2).getMatrixToArray();
        double[][] expected = matrixRes.getMatrixToArray();
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[0].length; j++) {
                assertEquals(expected[i][j],actual[i][j], INACCURACY_VALUE);
            }
        }
    }

    @Test
    public void testMatrixMultiplication2() {
        Matrix matrixMul1 = new Matrix(matrix2);
        Matrix matrixMul2 = new Matrix(matrix3);
        Matrix matrixRes = new Matrix(matrix2AndMatrix3);

        double[][] actual = matrixMul1.multiplication(matrixMul2).getMatrixToArray();
        double[][] expected = matrixRes.getMatrixToArray();
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[0].length; j++) {
                assertEquals(expected[i][j],actual[i][j], INACCURACY_VALUE);
            }
        }
    }

    @Test
    public void testMatrixParallelMultiplication1() throws Exception {
        Matrix matrixMul1 = new Matrix(matrix1);
        Matrix matrixMul2 = new Matrix(matrix1);
        Matrix matrixRes = new Matrix(matrix1AndMatrix1);

        double[][] actual = matrixMul1.multiplication(matrixMul2, Matrix.ParallelType.Standard).getMatrixToArray();
        double[][] expected = matrixRes.getMatrixToArray();
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[0].length; j++) {
                assertEquals(expected[i][j],actual[i][j], INACCURACY_VALUE);
            }
        }
    }

    @Test
    public void testMatrixParallelMultiplication2() throws Exception {
        Matrix matrixMul1 = new Matrix(matrix2);
        Matrix matrixMul2 = new Matrix(matrix3);
        Matrix matrixRes = new Matrix(matrix2AndMatrix3);

        double[][] actual = matrixMul1.multiplication(matrixMul2, Matrix.ParallelType.Standard).getMatrixToArray();
        double[][] expected = matrixRes.getMatrixToArray();
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[0].length; j++) {
                assertEquals(expected[i][j],actual[i][j], INACCURACY_VALUE);
            }
        }
    }

    @Test
    public void testMatrixMultiplicationTime() throws Exception {
        Matrix matrixMul1 = new Matrix(matrix2);
        Matrix matrixMul2 = new Matrix(matrix3);
        Matrix matrixRes = new Matrix(matrix2AndMatrix3);

        Matrix nonparallel = new Matrix(3, 3);
        Matrix parallel = new Matrix(3, 3);

        int iterations = 100000;

        double timeNonparallelStart = System.nanoTime();
        for (int i = 0; i < iterations; i++) {
            nonparallel = matrixMul1.multiplication(matrixMul2);
        }
        double timeNonparallelEnd = System.nanoTime();
        double timeNonparallel = timeNonparallelEnd - timeNonparallelStart;

        double timeParallelStart = System.nanoTime();
        for (int i = 0; i < iterations; i++) {
            parallel = matrixMul1.multiplication(matrixMul2, Matrix.ParallelType.Thread);
        }
        double timeParallelEnd = System.nanoTime();
        double timeParallel = timeParallelEnd - timeParallelStart;

        System.out.println("Time nonparallel = " + timeNonparallel + "; Time parallel = " + timeParallel + "; ");
        if (timeParallel < timeNonparallel) {
            System.out.println("timeParallel < timeNonParallel");
        } else {
            System.out.println("timeParallel > timeNonParallel");
        }

        for (int i = 0; i < nonparallel.getMatrixToArray().length; i++) {
            for (int j = 0; j < nonparallel.getMatrixToArray()[i].length; j++) {
                assertEquals(nonparallel.getMatrixToArray()[i][j], parallel.getMatrixToArray()[i][j], INACCURACY_VALUE);
            }
        }
    }

    @Test
    public void testMatrixTranspose1() {
        int rows = 3;
        int columns = 3;
        Matrix matrix = new Matrix(rows, columns);
        Matrix matrixExpected = new Matrix(rows, columns);

        matrix.setValue(new double[][] {{1, 2, 3}, {5.2, 5.3, 5.4}, {6.666, 7.777, 8.888}});
        matrixExpected.setValue(new double[][] {{1, 5.2, 6.666}, {2, 5.3, 7.777}, {3, 5.4, 8.888}});

        double[][] actual = matrix.transpose().getMatrixToArray();
        double[][] expected = matrixExpected.getMatrixToArray();
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[0].length; j++) {
                assertEquals(expected[i][j],actual[i][j], INACCURACY_VALUE);
            }
        }
    }

    @Test
    public void testMatrixTranspose2() {
        int rows = 3;
        int columns = 2;
        Matrix matrix = new Matrix(rows, columns);
        Matrix matrixExpected = new Matrix(columns, rows);

        matrix.setValue(new double[][] {{1, 2}, {3.3, 4.4}, {5.05, 6.06}});
        matrixExpected.setValue(new double[][] {{1, 3.3, 5.05}, {2, 4.4, 6.06}});

        double[][] actual = matrix.transpose().getMatrixToArray();
        double[][] expected = matrixExpected.getMatrixToArray();
        for (int i = 0; i < expected.length; i++) {
            for (int j = 0; j < expected[0].length; j++) {
                assertEquals(expected[i][j],actual[i][j], INACCURACY_VALUE);
            }
        }
    }
}
