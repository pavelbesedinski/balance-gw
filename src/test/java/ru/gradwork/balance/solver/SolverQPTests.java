package ru.gradwork.balance.solver;

//import org.junit.Before;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import ru.gradwork.balance.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SolverQPTests {
    public static final double INACCURACY_LIMIT = 1.0E-3;

    private BalanceSettings expectedBalanceSettings = null;
    private BalanceInput expectedBalanceInput = null;
    private final double[] expected = new double[]{10.056, 3.014, 7.041, 1.982, 5.059, 4.067, 0.992};

//    @Before
    public void testSolverQPInit() {
        // Setting for expectedBalanceSetting
        this.expectedBalanceSettings = new BalanceSettings();
        expectedBalanceSettings.setRoundUnit(0.01);
        expectedBalanceSettings.setBalanceSettingsConstraints(BalanceSettingsConstraints.METROLOGIC);

        BalanceInputVariable[] expectedBalanceInputVariable = new BalanceInputVariable[7];
        for (int i = 0; i < 7; i++) {
            expectedBalanceInputVariable[i] = new BalanceInputVariable();
        }
        expectedBalanceInputVariable[0].setTestValues(String.valueOf(1), null, String.valueOf(1), 10.005,  new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.2, true, false);
        expectedBalanceInputVariable[1].setTestValues(String.valueOf(2), String.valueOf(1), null, 3.033, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.121, true, false);
        expectedBalanceInputVariable[2].setTestValues(String.valueOf(3), String.valueOf(1), String.valueOf(2), 6.831, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.683, true, false);
        expectedBalanceInputVariable[3].setTestValues(String.valueOf(4), String.valueOf(2), null, 1.985, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.04, true, false);
        expectedBalanceInputVariable[4].setTestValues(String.valueOf(5), String.valueOf(2), String.valueOf(3), 5.093, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.102, true, false);
        expectedBalanceInputVariable[5].setTestValues(String.valueOf(6), String.valueOf(3), null, 4.057, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.081, true, false);
        expectedBalanceInputVariable[6].setTestValues(String.valueOf(7), String.valueOf(3), null, 0.991, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.02, true, false);

        // Setting for expectedBalanceInputVariable
        ArrayList<BalanceInputVariable> expectedBalanceInputVariableList = new ArrayList<>();
        expectedBalanceInputVariableList.addAll(Arrays.asList(expectedBalanceInputVariable).subList(0, 7));

        // Setting for expectedBalanceInput
        this.expectedBalanceInput = new BalanceInput();
        expectedBalanceInput.setBalanceSettings(expectedBalanceSettings);
        expectedBalanceInput.setBalanceInputVariables(expectedBalanceInputVariableList);
    }

    @Test
    public void testSolverQPWithJOptimizerSolver() throws Exception {
        testSolverQPInit();
        SolverQP solverQP = new SolverQP(expectedBalanceInput);
        BalanceOutput balanceOutput = solverQP.solve(expectedBalanceSettings, SolverMode.JOptimizer);
        List<Double> actualValuesList = new ArrayList<Double>();
        for (int i = 0; i < balanceOutput.getBalanceOutputVariables().size(); i++) {
            actualValuesList.add(balanceOutput.getBalanceOutputVariables().get(i).getValue());
        }
        Double[] actual = new Double[actualValuesList.size()];
        actualValuesList.toArray(actual);
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i],actual[i], INACCURACY_LIMIT);
        }
    }

    @Test
    public void testSolverQPWithOjAlgoSolver() throws Exception {
        testSolverQPInit();
        SolverQP solverQP = new SolverQP(expectedBalanceInput);
        BalanceOutput balanceOutput = solverQP.solve(expectedBalanceSettings, SolverMode.OjAlgo);
        List<Double> actualValuesList = new ArrayList<Double>();
        for (int i = 0; i < balanceOutput.getBalanceOutputVariables().size(); i++) {
            actualValuesList.add(balanceOutput.getBalanceOutputVariables().get(i).getValue());
        }

        Double[] actual = new Double[actualValuesList.size()];
        actualValuesList.toArray(actual);
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i],actual[i], INACCURACY_LIMIT);
        }
    }

    @Test
    @Disabled
    public void testSolverQPWithMatLabSolver() throws Exception {
        testSolverQPInit();
        SolverQP solverQP = new SolverQP(expectedBalanceInput);
        BalanceOutput balanceOutput = solverQP.solve(expectedBalanceSettings, SolverMode.MATLAB);
        List<Double> actualValuesList = new ArrayList<Double>();
        for (int i = 0; i < balanceOutput.getBalanceOutputVariables().size(); i++) {
            actualValuesList.add(balanceOutput.getBalanceOutputVariables().get(i).getValue());
        }
        Double[] actual = new Double[actualValuesList.size()];
        actualValuesList.toArray(actual);
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i],actual[i], INACCURACY_LIMIT);
        }
    }

    @Test
    @Disabled
    public void testSolverQPWithGurobiSolver() throws Exception {
        testSolverQPInit();
        SolverQP solverQP = new SolverQP(expectedBalanceInput);
        BalanceOutput balanceOutput = solverQP.solve(expectedBalanceSettings, SolverMode.Gurobi);
        List<Double> actualValuesList = new ArrayList<Double>();
        for (int i = 0; i < balanceOutput.getBalanceOutputVariables().size(); i++) {
            actualValuesList.add(balanceOutput.getBalanceOutputVariables().get(i).getValue());
        }
        Double[] actual = new Double[actualValuesList.size()];
        actualValuesList.toArray(actual);
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i],actual[i], INACCURACY_LIMIT);
        }
    }
}
