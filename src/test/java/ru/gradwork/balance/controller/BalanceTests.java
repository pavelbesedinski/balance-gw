package ru.gradwork.balance.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.gradwork.balance.model.*;

import java.util.ArrayList;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class BalanceTests {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final String URL_TEMPLATE_POST = "/balance/post/";

    private BalanceInput expectedBalanceInput = null;

    @Autowired
    private MockMvc mockMvc;

    public void getBalanceTestInit() {
        BalanceInputVariable[] expectedBalanceInputVariable = new BalanceInputVariable[7];

        for (int i = 0; i < 7; i++) {
            expectedBalanceInputVariable[i] = new BalanceInputVariable();
        }
        expectedBalanceInputVariable[0].setTestValues(String.valueOf(1), null, String.valueOf(1), 10.005,  new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.2, true, false);
        expectedBalanceInputVariable[1].setTestValues(String.valueOf(2), String.valueOf(1), null, 3.033, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.121, true, false);
        expectedBalanceInputVariable[2].setTestValues(String.valueOf(3), String.valueOf(1), String.valueOf(2), 6.831, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.683, true, false);
        expectedBalanceInputVariable[3].setTestValues(String.valueOf(4), String.valueOf(2), null, 1.985, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.04, true, false);
        expectedBalanceInputVariable[4].setTestValues(String.valueOf(5), String.valueOf(2), String.valueOf(3), 5.093, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.102, true, false);
        expectedBalanceInputVariable[5].setTestValues(String.valueOf(6), String.valueOf(3), null, 4.057, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.081, true, false);
        expectedBalanceInputVariable[6].setTestValues(String.valueOf(7), String.valueOf(3), null, 0.991, new BalanceConstraints(0, 10000), new BalanceConstraints(0, 10000), 0.02, true, false);

        // Setting for expectedBalanceInputVariable
        ArrayList<BalanceInputVariable> expectedBalanceInputVariableList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            expectedBalanceInputVariableList.add(expectedBalanceInputVariable[i]);
        }

        // Setting for expectedBalanceSetting
        BalanceSettings expectedBalanceSettings = new BalanceSettings();
        expectedBalanceSettings.setRoundUnit(0.01);
        expectedBalanceSettings.setBalanceSettingsConstraints(BalanceSettingsConstraints.METROLOGIC);
        // Setting for expectedBalanceInput
        expectedBalanceInput = new BalanceInput();
        expectedBalanceInput.setBalanceSettings(expectedBalanceSettings);
        expectedBalanceInput.setBalanceInputVariables(expectedBalanceInputVariableList);
    }

    @Test
    public void getBalanceJOptimizerAPITest() throws Exception {
        getBalanceTestInit();
        MvcResult result = mockMvc
                .perform(post(URL_TEMPLATE_POST)
                        .contentType("application/json")
                        .content(objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                                .writeValueAsString(expectedBalanceInput))
                        .header("Solver", "JOptimizer"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void getBalanceOjAlgoAPITest() throws Exception {
        getBalanceTestInit();
        MvcResult result = mockMvc
                .perform(post(URL_TEMPLATE_POST)
                        .contentType("application/json")
                        .content(objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                                .writeValueAsString(expectedBalanceInput))
                        .header("Solver", "OjAlgo"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();
    }

//    @Test
//    public void getBalanceMatlabAPITest() throws Exception {
//        getBalanceTestInit();
//        MvcResult result = mockMvc
//                .perform(post(URL_TEMPLATE_POST)
//                        .contentType("application/json")
//                        .content(objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
//                                .writeValueAsString(expectedBalanceInput))
//                        .header("Solver", "MatLab"))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andReturn();
//    }

//    @Test
//    public void getBalanceGurobiAPITest() throws Exception {
//        getBalanceTestInit();
//        MvcResult result = mockMvc
//                .perform(post(URL_TEMPLATE_POST)
//                        .contentType("application/json")
//                        .content(objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
//                                .writeValueAsString(expectedBalanceInput))
//                        .header("Solver", "Gurobi"))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andReturn();
//    }
}
